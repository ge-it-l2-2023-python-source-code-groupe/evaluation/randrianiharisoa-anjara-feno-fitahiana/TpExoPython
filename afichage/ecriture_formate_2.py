def ecriture_formate_2():
    print("ecriture formater 2")
    # Calcul du pourcentage de GC
    perc_GC = ((4500 + 2575) / 14800) * 100

    # Affichage avec écriture formatée
    print(f"Le pourcentage de GC est {round(perc_GC)}%")
    print(f"Le pourcentage de GC est {perc_GC:.1f}%")
    print(f"Le pourcentage de GC est {perc_GC:.2f}%")
    print(f"Le pourcentage de GC est {perc_GC:.3f} %")
