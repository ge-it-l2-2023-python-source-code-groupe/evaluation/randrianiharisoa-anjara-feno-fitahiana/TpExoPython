def nombre_premier_inf_100():
    print("nombre premier inferieur a 100")
    print("methode 1")
    nombres_premiers = []

    for nombre in range(2, 101):
        est_premier = True
        for diviseur in range(2, int(nombre**0.5) + 1):
            if nombre % diviseur == 0:
                est_premier = False
                break
        if est_premier:
            nombres_premiers.append(nombre)

    return nombres_premiers

    resultat_methode1 = methode1()
    print(f"Les nombres premiers inférieurs à 100 (méthode 1) : {resultat_methode1}")
    print(f"Il y a {len(resultat_methode1)} nombres premiers dans cette plage.")


    print("methode 2")

    nombres_premiers = [2]

    for nombre in range(3, 101):
        est_premier = True
        for diviseur in nombres_premiers:
            if diviseur > int(nombre**0.5) + 1:
                break
            if nombre % diviseur == 0:
                est_premier = False
                break
        if est_premier:
            nombres_premiers.append(nombre)

    return nombres_premiers

    resultat_methode2 = methode2()
    print(f"Les nombres premiers inférieurs à 100 (méthode 2) : {resultat_methode2}")
    print(f"Il y a {len(resultat_methode2)} nombres premiers dans cette plage.")
