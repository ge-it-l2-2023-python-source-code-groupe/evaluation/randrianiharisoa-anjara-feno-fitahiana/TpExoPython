def conjection_de_syracuse():
    print ("conjection de syracuse")
    def syracuse(n, iterations=100):
        sequence = [n]

        for _ in range(iterations):
            if n % 2 == 0:
                n = n // 2
            else:
                n = 3 * n + 1
            sequence.append(n)

            if n == 1:
                break

        return sequence

    def main():
        points_de_depart = [10, 20, 5, 3, 7]  # Vous pouvez ajouter d'autres points de départ
        iterations = 100

        for point_de_depart in points_de_depart:
            sequence = syracuse(point_de_depart, iterations)
            print(f"Suite de Syracuse pour n={point_de_depart}: {sequence}")

            if sequence[-1] == 1:
                print(f"La conjecture de Syracuse est vérifiée pour n={point_de_depart}.")
            else:
                print(f"La conjecture de Syracuse n'est pas vérifiée pour n={point_de_depart}.")

            cycle_trivial = sequence[sequence.index(1) + 1:]
            print(f"Cycle trivial pour n={point_de_depart}: {cycle_trivial}\n")

    if __name__ == "__main__":
        main()
