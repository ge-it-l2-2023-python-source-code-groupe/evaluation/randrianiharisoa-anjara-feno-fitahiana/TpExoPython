def recherche_nombre_dicotomie():
    print ("recherche nombre dicotomie")
    print("Pensez à un nombre entre 1 et 100.")

    # Initialiser les bornes de l'intervalle
    borne_min = 1
    borne_max = 100

    # Initialiser le nombre de tentatives
    tentatives = 0

    while True:
        # Calculer la proposition en prenant la moitié de l'intervalle
        proposition = (borne_min + borne_max) // 2

        # Poser la question à l'utilisateur
        reponse = input(f"Est-ce votre nombre est plus grand, plus petit ou égal à {proposition} ? [+/-/=] ")

        # Incrémenter le nombre de tentatives
        tentatives += 1

        if reponse == "+":
            borne_min = proposition + 1
        elif reponse == "-":
            borne_max = proposition - 1
        elif reponse == "=":
            print(f"J'ai trouvé en {tentatives} questions !")
            break
        else:
            print("Veuillez entrer +, - ou =.")

