def minimum_d_un_liste():
    print("minimun d un liste")
    # Liste d'entiers
    liste_entiers = [8, 4, 6, 1, 5]

    # Recherche du plus petit élément sans utiliser min()
    plus_petit = liste_entiers[0]
    for element in liste_entiers:
        if element < plus_petit:
            plus_petit = element

    print(f"Le plus petit élément de la liste est : {plus_petit}")

    # Séquence d'acides aminés
    sequence_amino_acides = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]

    # Calcul de la fréquence des acides aminés
    frequence_A = sequence_amino_acides.count("A")
    frequence_R = sequence_amino_acides.count("R")
    frequence_W = sequence_amino_acides.count("W")
    frequence_G = sequence_amino_acides.count("G")

    # Affichage des résultats
    print(f"Fréquence de l'acide aminé alanine (A) : {frequence_A}")
    print(f"Fréquence de l'acide aminé arginine (R) : {frequence_R}")
    print(f"Fréquence de l'acide aminé tryptophane (W) : {frequence_W}")
    print(f"Fréquence de l'acide aminé glycine (G) : {frequence_G}")
