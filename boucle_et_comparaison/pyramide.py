def pyramide ():
    print("pyramide")
    # Demander à l'utilisateur le nombre de lignes de la pyramide
    reponse = input("Entrez un nombre de lignes (entier positif) : ")

    # Vérifier que la réponse est un entier positif
    while not reponse.isdigit() or int(reponse) <= 0:
        print("Veuillez entrer un entier positif.")
        reponse = input("Entrez un nombre de lignes (entier positif) : ")

    N = int(reponse)

    # Dessiner la pyramide
    for i in range(1, N + 1):
        espaces = ' ' * (N - i)
        etoiles = '*' * (2 * i - 1)
        ligne = espaces + etoiles
        print(ligne)
