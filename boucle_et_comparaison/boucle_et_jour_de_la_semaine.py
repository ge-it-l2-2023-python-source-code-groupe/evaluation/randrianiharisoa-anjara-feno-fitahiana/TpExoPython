def boucle_et_jour_de_la_semaine ():
    print("Boucle et jours de la semaine")
    # Liste des jours de la semaine
    semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]

    # Affichage des jours de la semaine avec une boucle for
    print("Jours de la semaine avec une boucle for:")
    for jour in semaine:
        print(jour)

    # Affichage des jours du week-end avec une boucle while
    print("\nJours du week-end avec une boucle while:")
    index = 5  # L'index correspondant à Samedi
    while index < len(semaine):
        print(semaine[index])
        index += 1
