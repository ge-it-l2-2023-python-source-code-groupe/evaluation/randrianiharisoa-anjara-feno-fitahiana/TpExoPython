def calcule_de_la_moyenne ():
    print("Calcul de la moyenne")
    # Liste des notes de l'étudiant
    notes = [14, 9, 6, 8, 12]

    # Calcul de la moyenne
    moyenne = sum(notes) / len(notes)

    # Affichage de la moyenne avec deux décimales
    print("La moyenne est : {:.2f}".format(moyenne))
