def status_de_puce ():
    print("saut de puce")
    import random

    # Position initiale de la puce
    position = 0

    # Position finale souhaitée
    position_finale = 5

    # Compteur de sauts
    nombre_de_sauts = 0

    # Boucle de simulation du mouvement de la puce
    while position != position_finale:
        # Effectuer un saut aléatoire
        saut = random.choice([-1, 1])
        position += saut

        # Incrémenter le compteur de sauts
        nombre_de_sauts += 1

        # Affichage de la position actuelle de la puce
        print(f"Position actuelle : {position}")

    # Affichage du nombre total de sauts nécessaires
    print(f"\nNombre total de sauts nécessaires : {nombre_de_sauts}")
