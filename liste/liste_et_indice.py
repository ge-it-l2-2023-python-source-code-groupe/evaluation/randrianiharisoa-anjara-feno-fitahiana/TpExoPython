def liste_et_indice():
    print("liste et indice")
    # Création de la liste des jours de la semaine
    semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]

    # 1. Afficher la liste semaine
    print("1. Liste semaine :", semaine)

    # 2. Afficher la valeur de semaine[4]
    print("2. Valeur de semaine[4] :", semaine[4])

    # 3. Échanger les valeurs de la première et de la dernière case de cette liste
    semaine[0], semaine[-1] = semaine[-1], semaine[0]
    print("3. Liste après échange de la première et de la dernière case :", semaine)

    # 4. Afficher 12 fois la valeur du dernier élément de la liste
    dernier_element = semaine[-1]
    print("4. Afficher 12 fois la valeur du dernier élément de la liste:")
    for _ in range(12):
        print(dernier_element)
