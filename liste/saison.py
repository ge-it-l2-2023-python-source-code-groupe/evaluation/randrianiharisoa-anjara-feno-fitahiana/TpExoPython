def saison ():
    print("saison")
    # Création des listes des mois pour chaque saison
    hiver = ["Décembre", "Janvier", "Février"]
    printemps = ["Mars", "Avril", "Mai"]
    ete = ["Juin", "Juillet", "Août"]
    automne = ["Septembre", "Octobre", "Novembre"]

    # Création de la liste des saisons
    saisons = [hiver, printemps, ete, automne]

    # 1. Accéder à la liste correspondant à la troisième saison (été)
    resultat1 = saisons[2]
    print("1. Saisons[2] :", resultat1)

    # 2. Accéder au premier mois de la deuxième saison (printemps)
    resultat2 = saisons[1][0]
    print("2. Saisons[1][0] :", resultat2)

    # 3. Accéder à une sous-liste contenant la deuxième saison (printemps)
    resultat3 = saisons[1:2]
    print("3. Saisons[1:2] :", resultat3)

    # 4. Accéder au deuxième élément de chaque saison
    # Note: Cette expression ne produit pas le résultat attendu
    resultat4 = saisons[:][1]
    print("4. Saisons[:][1] :", resultat4)
