from variables import friedman,operation,operationEtConversionDeType
from afichage import addition,poly_A,poly_A_et_GC,ecriture_formate,ecriture_formate_2
from liste import jour_de_la_semaine,table_de_multiplication,nombre_paire,saison,liste_et_indice,liste_et_range
from teste import acide_amine,complementaire_d_un_ADN,conjection_de_syracuse,jour_de_la_semaine,minimum_d_un_liste,nombre_paire,nombre_premier_inf_100,note_et_mention_d_un_etudiant,recherche_nombre_dicotomie,acide_amine_d_un_proteine
from boucle_et_comparaison import fibonacy, boucle_de_base,boucle_et_jour_de_la_semaine,calcule_de_la_moyenne,fibonacy,nombre_paire_et_impaire,nombre_sur_une_ligne,parcour_demi_matrice,produit_de_nombre_consecutif,pyramide,status_de_puce,triangle_gauche,triangle_inverse,triangle,parcours_de_matrice

def init():
    choice="1"
    while (choice!="0"):
        print("***1***-------variable-------\n***2***-------affichage------\n***3***---------liste--------\n***4***-boucle et comparaison-\n***5***----------test--------\n***STOP***-------QUITER-------")
        choice = input(print("entrer votre choix?\"STOP\"pour quiter!"))
        choice=str(choice)
        match choice:
            case "1":
                def variable():
                    print("\n\n\n------variable------\n***2.11.1***Nombre du friedman\n***2.11.2***prediction\n***2.11.3***conversion du type\n***\"MENU\" ou \"enter\"***-----revenir au menu-----")
                    choice = input(print("entrer votre choix?"))
                    choice=str(choice)
                    if(choice=="2.11.1"):
                        print("-----2.11.1-----")
                        friedman.friedman()
                        variable()
                    elif(choice=="2.11.2"):
                        print("----2.11.2------")
                        operation.operation()
                        variable()
                    elif(choice=="2.11.3"):
                        print("----2.11.3------")
                        operationEtConversionDeType.resultat()
                        variable()
                    elif(choice=="MENU"):
                        return
                
                variable()
            case "2":
                def affichage():
                    print("------affichage-------\n***3.6.1***affichage resultat de l addition\n***3.6.2***poly-A\n***3.6.3***poly-A et poly G-C\n***3.6.4***ecriture formate\n***3.6.5***ecriture formater2\n***\"MENU\" ou \"enter\"***-----revenir au menu-----")
                    choice = input(print("entrer votre choix?"))
                    choice=str(choice)   
                    if(choice=="3.6.1"):
                        print("---3.6.1---")
                        addition.addition()
                        affichage()
                    elif(choice=="3.6.2"):
                        print("---3.6.2---")
                        poly_A.adn()
                        affichage()
                    elif(choice=="3.6.3"):
                        print("---3.6.3---")
                        poly_A_et_GC.poly_A_et_poly_GC()
                        affichage()
                    elif(choice=="3.6.4"):
                        print("---3.6.4---")
                        ecriture_formate.ecriture_formate()
                        affichage()
                    elif(choice=="3.6.5"):
                        print("---3.6.5---")
                        ecriture_formate_2.ecriture_formate_2()
                        affichage()
                    elif(choice=="MENU"):
                        return
                affichage()
            case "3":
                def liste():
                    print("------liste--------\n***4.10.1***jour de la semaine\n***4.10.2***saison\n***4.10.3***table de multiplication\n***4.10.4***nombre paire\n***4.10.5***liste and indixe\n***4.10.6***liste and range\n***\"MENU\" ou \"enter\"***-----revenir au menu-----")
                    choice = input(print("entrer votre choix?"))
                    choice=str(choice)
                    if(choice=="4.10.1"):
                        print("---4.10.1")
                        jour_de_la_semaine.jour_de_la_semaine()
                        liste()
                    elif(choice=="4.10.2"):
                        print("---4.10.2---")
                        saison.saison()
                        liste()
                    elif(choice=="4.10.3"):
                        print("---4.10.3---")
                        table_de_multiplication.table_de_multiplication()
                        liste()
                    elif(choice=="4.10.4"):
                        print("---4.10.4---")
                        nombre_paire.nombre_paire()
                        liste()
                    elif(choice=="4.10.5"):
                        print("---4.10.5---")
                        liste_et_indice.liste_et_indice()
                        liste()
                    elif(choice=="4.10.6"):
                        print("---4.10.6---")
                        liste_et_range.liste_et_range()
                        liste()
                    elif(choice=="MENU"):
                        return
                liste()
            case "4":
                def boucle_et_comparaison():
                    print("------boucle et comparaison------\n***5.4.1***bocle de base\n***5.4.2***boucle et jour de la semaine\n***5.4.3***nombre de 1 a 10 sur une ligne\n***5.4.4***nombre paire et impaire\n***5.4.5***calcul de moyenne\n***5.4.6***produit nombre consecutif\n***5.4.7***triangle\n***5.4.8***triangle inverser\n***5.4.9***triangle gauche\n***5.4.10***pyramide\n***5.4.11***parcour de matrice\n***5.4.12***parcour de demi matrice\n***5.4.13***saut de puce\n***5.4.14***suite de fibonacci\n***\"MENU\" ou \"enter\"***-----revenir au menu-----")
                    choice = input(print("entrer votre choix?"))
                    choice=str(choice)
                    if(choice=="5.4.1"):
                        print("---5.4.1---")
                        boucle_de_base.boucle_de_base()
                        boucle_et_comparaison()
                    elif(choice=="5.4.2"):
                        print("---5.4.2---")
                        boucle_et_jour_de_la_semaine.boucle_et_jour_de_la_semaine()
                        boucle_et_comparaison()
                    elif(choice=="5.4.3"):
                        print("---5.4.3---")
                        nombre_sur_une_ligne.nombre_sur_une_ligne()
                        boucle_et_comparaison()
                    elif(choice=="5.4.4"):
                        print("---5.4.4---")
                        nombre_paire_et_impaire.nombre_paire_et_impaire()
                        boucle_et_comparaison()
                    elif(choice=="5.4.5"):
                        print("---5.4.5---")
                        calcule_de_la_moyenne.calcule_de_la_moyenne()
                        boucle_et_comparaison()
                    elif(choice=="5.4.6"):
                        print("---5.4.6---")
                        produit_de_nombre_consecutif.produit_de_nombre_consecutif()
                        boucle_et_comparaison()
                    elif(choice=="5.4.7"):
                        print("---5.4.7---")
                        triangle.triangle()
                        boucle_et_comparaison()
                    elif(choice=="5.4.8"):
                        print("---5.4.8---")
                        triangle_inverse.triangle_inverse()
                        boucle_et_comparaison()
                    elif(choice=="5.4.9"):
                        print("---5.4.9---")
                        triangle_gauche.triangle_gauche()
                        boucle_et_comparaison()
                    elif(choice=="5.4.10"):
                        print("---5.4.10---")
                        pyramide.pyramide()
                        boucle_et_comparaison()
                    elif(choice=="5.4.11"):
                        print("---5.4.11---")
                        parcours_de_matrice.parcours_de_matrice()
                        boucle_et_comparaison()
                    elif(choice=="5.4.12"):
                        print("---5.4.12---")
                        parcour_demi_matrice.paarcour_demi_matrice()
                        boucle_et_comparaison()
                    elif(choice=="5.4.13"):
                        print("---5.4.13---")
                        status_de_puce.status_de_puce()
                        boucle_et_comparaison()
                    elif(choice=="5.4.14"):
                        print("---5.4.14---")
                        fibonacy.fibonacy()
                        boucle_et_comparaison()
                    elif(choice=="MENU"):
                        return
                boucle_et_comparaison()
            case "5":
                def teste():
                    print("------test--------\n***6.7.1***jour de la semaine\n***5.7.2***Sequence complementaire d'un brin d'ADN\n***6.7.3*** Minimum d'une liste\n***6.7.4*** Fréquence des acides aminés\n***6.7.5***Notes et mention d'un étudiant\n***6.7.6*** Nombres pairs\n***6.7.7*** Conjecture de Syracuse\n***6.7.8***Attribution de la structure secondaire des acides aminés d'une protéine (exercice +++)\n***6.7.9*** Détermination des nombres premiers inférieurs à 100\n***6.7.10***Recherche d'un nombre par dichotomie\n***\"MENU\" ou \"enter\"***-----revenir au menu-----")
                    choice = input(print("entrer votre choix?"))
                    choice=str(choice)
                    if(choice=="6.7.1"):
                        print("---6.7.1---")
                        jour_de_la_semaine.jour_de_la_semaine()
                        teste()
                    elif(choice=="6.7.2"):
                        print("---6.7.2---")
                        complementaire_d_un_ADN.complementaire_d_un_ADN()
                        teste()
                    elif(choice=="6.7.3"):
                        print("---6.7.3---")
                        minimum_d_un_liste.minimum_d_un_liste()
                        teste()
                    elif(choice=="6.7.4"):
                        print("---6.7.4---")
                        acide_amine.acide_amine()
                        teste()
                    elif(choice=="6.7.5"):
                        print("---6.7.5---")
                        note_et_mention_d_un_etudiant.note_et_mention_d_un_etudiant()
                        teste()
                    elif(choice=="6.7.6"):
                        print("---6.7.6---")
                        nombre_paire.nombre_paire()
                        teste()
                    elif(choice=="6.7.7"):
                        print("---6.7.7---")
                        conjection_de_syracuse.conjection_de_syracuse()
                        teste()
                    elif(choice=="6.7.8"):
                        print("---6.7.8---")
                        acide_amine_d_un_proteine.acide_amine_d_un_proteine()
                        teste()
                    elif(choice=="6.7.9"):
                        print("---6.7.9---")
                        nombre_premier_inf_100.nombre_premier_inf_100()
                        teste()
                    elif(choice=="6.7.10"):
                        print("---6.7.10---")
                        recherche_nombre_dicotomie.recherche_nombre_dicotomie()
                        teste()
                    elif(choice=="MENU"):
                        return
                teste()
            case "STOP":
                break

def main():
    init()

if __name__ == "__main__":
    init()

